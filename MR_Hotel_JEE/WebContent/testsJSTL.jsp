<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="Beans.*"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<link rel="stylesheet" href="styleJSTL.css"/>
	</head>
	
	<body>
	
		<div id="fenetre">
		
			<div id="recherche">
			
				<form method="post" action="testJSTLServlet">
				
					<label for="rechercheIdHotel">Numero d'un hotel</label>
					<input type="text" id="rechercheIDHotel" name="rechercheIDHotel"/>
			
					<input type="submit" value="Valider" />
					
				</form>
				
			</div>
			
			
			<br/>
			
			
			<div id="affichageHotel">
				
				<table>
					<caption>Hotel Sélectionné:</caption>
					<tr>
						<td><c:out value="${beanHotel.numeroHotel}" /></td>
						<td><c:out value="${beanHotel.nomHotel}" /></td>
						<td><c:out value="${beanHotel.adresseHotel}" /></td>
						<td><c:out value="${beanHotel.codePostalHotel}" /></td>
						<td><c:out value="${beanHotel.villeHotel}" /></td>
					</tr>
					
				</table>
				
			</div>
			
			
			<br/>
			
			
			<div id="affichageChambres">
				
				<table>
					<caption>Chambres de l'hotel:</caption>
					
					<c:forEach items="${requestScope.listeChambres}" var="chambre">
					
						<tr>
							<td><c:out value="${chambre.numeroDeChambre}" /></td>
							<td><c:out value="${chambre.categorieChambre.getNomCategorie()}" /></td>
						</tr>
						
					</c:forEach>
					
				</table>
				
			</div>
			
		</div>
	
	</body>

</html>