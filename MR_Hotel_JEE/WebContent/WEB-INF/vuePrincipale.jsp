<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, Beans.Filtre, Beans.Hotel"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<link rel="stylesheet" href="style.css"/>
	</head>
	
	<body>
	
		<%--bloc fenetre --%>
		<div id="fenetre">
	
			<%--bloc Filtres --%>
			<div id="affichageFiltres">
			
				<form method="get" action="ActionFiltre">
					
					
					
					<%
					//Recuperation de la liste de filtres dans la requete
					ArrayList<Filtre> listeFiltres = (ArrayList<Filtre>) request.getAttribute("listeFiltres");
					
					//Pour chaque filtre
					for (Filtre filtre : listeFiltres) {
						
						//affichage du titre du filtre
						%><p><%out.println(filtre.getTitre()); %></p><%
						
						//je récupère les noms des checkboxes dans une liste
						ArrayList<String> liste = (ArrayList<String>) filtre.getListeChampARetourner();
						
						//Pour chaque nom dans la liste
						for (String str : liste ) {
							%>
							<%--Je créeé une checkbox + un label associé --%>
							<input type="checkbox" name="<%out.print(str);%>" value="value"/>
							<label for="<%out.println(str);%>" ><%out.println(str);%></label>
							<br/>
							<%
					    }%>
					  
						<br/>
					<%
					}%>
					
					<input type="submit" name="validation" value="Valider">
					
				</form>
				
				<%--Stockage de la liste de filtres dans session --%>
				<%request.getSession().setAttribute("listeFiltres", listeFiltres); %>
			
			</div>
			
			
			<%--Bloc table Hotel --%>
			<div id="affichageTable">
				
				<%--Recuperation de la liste d'hotels a afficher --%>
				<%ArrayList<Hotel> listeDesHotels = (ArrayList<Hotel>) request.getAttribute("listeHotels"); %>
				
				<table>
				
					<%
					//Pour chaque hotel je créé une ligne
					for (Hotel hotelEnCours : listeDesHotels) {
						%>
						<tr>
							<%
							//Recuperation des éléments a afficher pour un hotel
							ArrayList<String> ligneHotel = (ArrayList<String>) hotelEnCours.getArrayListAttributs();
							
							//pour chaque élément a afficher
							for (String contenu : ligneHotel) {
								
								//creation de colonnes
								%>
								<td><%out.println(contenu);%></td>
								<%
							}
							%>
						</tr>
					<%}
					%>
					
				</table>
				
			</div>
		
		</div>
		
	</body>

</html>