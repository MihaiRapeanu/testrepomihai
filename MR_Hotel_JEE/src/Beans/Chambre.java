package Beans;

public class Chambre {
	
	//Attributs
	private int 				numeroDeChambre;
	private CategorieChambre 	categorieChambre;
	private Hotel 				hotelAssoChambre;
	
	//Constructeur
	public Chambre() {
		
	}
	
	public Chambre(int p_NumeroDeChambre, CategorieChambre p_CategorieChambre, Hotel p_HotelAssoChambre){
		this.numeroDeChambre 	= p_NumeroDeChambre;
		this.categorieChambre 	= p_CategorieChambre;
		this.hotelAssoChambre 	= p_HotelAssoChambre;
	}

	//G & S
	public int getNumeroDeChambre() {
		return numeroDeChambre;
	}

	public void setNumeroDeChambre(int numeroDeChambre) {
		this.numeroDeChambre = numeroDeChambre;
	}

	public CategorieChambre getCategorieChambre() {
		return categorieChambre;
	}

	public void setCategorieChambre(CategorieChambre categorieChambre) {
		this.categorieChambre = categorieChambre;
	}

	public Hotel getHotelAssoChambre() {
		return hotelAssoChambre;
	}

	public void setHotelAssoChambre(Hotel hotelAssoChambre) {
		this.hotelAssoChambre = hotelAssoChambre;
	}
	
	//Méthodes
}
