package Beans;

public class CategorieChambre {
	
	//Attributs
	private int 	numeroCategorie;
	private String	nomCategorie;
	
	//Constructeur
	public CategorieChambre (){
		
	}
	
	public CategorieChambre (int p_NumeroCategorie, String p_NomCategorie){
		this.numeroCategorie 	= p_NumeroCategorie;
		this.nomCategorie 	= p_NomCategorie;
	}

	//G & S
	public int getNumeroCategorie() {
		return numeroCategorie;
	}

	public void setNumeroCategorie(int numeroCategorie) {
		this.numeroCategorie = numeroCategorie;
	}

	public String getNomCategorie() {
		return nomCategorie;
	}

	public void setNomCategorie(String nomCategorie) {
		this.nomCategorie = nomCategorie;
	}
	
	//Méthodes
	
	
}	
