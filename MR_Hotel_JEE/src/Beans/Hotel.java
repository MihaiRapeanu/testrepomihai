package Beans;

import java.util.ArrayList;

public class Hotel {
	
	//Attributs
	private int 		numeroHotel;
	private TypeHotel 	typeHotel;
	private String 		nomHotel;
	private String 		adresseHotel;
	private String		codePostalHotel;
	private String 		villeHotel;
	
	//Constructeur
	public Hotel () {
		
	}
	
	public Hotel (int p_NumeroHotel, TypeHotel p_TypeHotel, String p_NomHotel, String p_AdresseHotel, String p_CodePostalHotel,
				String p_VilleHotel) {
		this.numeroHotel 		= p_NumeroHotel;
		this.typeHotel 			= p_TypeHotel;
		this.nomHotel 			= p_NomHotel;
		this.adresseHotel 		= p_AdresseHotel;
		this.codePostalHotel 	= p_CodePostalHotel;
		this.villeHotel 		= p_VilleHotel;
	}

	
	
	//G & S
	public int getNumeroHotel() {
		return numeroHotel;
	}

	public void setNumeroHotel(int numeroHotel) {
		this.numeroHotel = numeroHotel;
	}

	public TypeHotel getTypeHotel() {
		return typeHotel;
	}

	public void setTypeHotel(TypeHotel typeHotel) {
		this.typeHotel = typeHotel;
	}

	public String getNomHotel() {
		return nomHotel;
	}

	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}

	public String getAdresseHotel() {
		return adresseHotel;
	}

	public void setAdresseHotel(String adresseHotel) {
		this.adresseHotel = adresseHotel;
	}

	public String getCodePostalHotel() {
		return codePostalHotel;
	}

	public void setCodePostalHotel(String codePostalHotel) {
		this.codePostalHotel = codePostalHotel;
	}

	public String getVilleHotel() {
		return villeHotel;
	}

	public void setVilleHotel(String villeHotel) {
		this.villeHotel = villeHotel;
	}

	public int getDepartementHotel() {
		return Integer.parseInt(this.codePostalHotel.substring(0, 2));
	}

	//Méthodes
	
	//Permet de récuperer les valeurs de certains attributs de l'hotel sous forme de liste de String (pour affichage table de la vue)
	public ArrayList<String> getArrayListAttributs() {
		
		ArrayList<String> listeRetour = new ArrayList<String>();
		
		listeRetour.add(this.nomHotel);
		listeRetour.add((String) this.typeHotel.getNomTypeHotel());
		listeRetour.add(this.adresseHotel);
		listeRetour.add(this.codePostalHotel);
		listeRetour.add(this.villeHotel);
		
		return listeRetour;
		
	}

}
