package Beans;

import java.util.ArrayList;

import DAO.DAOFactory;

public class Filtre {
	
	//Attributs
	ArrayList<String> listeAffichageFiltre;
	String titre;
	String table;
	String champ;
		
	
	//Constructeur
	public Filtre() {
		
	}
	
	
	//G & S
	public String getTable() {
		return table;
	}


	public void setTable(String table) {
		this.table = table;
	}


	public String getChamp() {
		return champ;
	}


	public void setChamp(String champ) {
		this.champ = champ;
	}
	
	public ArrayList<String> getListeChampARetourner() {
		return listeAffichageFiltre;
	}

	public String getTitre() {
		return titre;
	}

	public void setListeChampARetourner(ArrayList<String> listeChampARetourner) {
		this.listeAffichageFiltre = listeChampARetourner;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}

	
	//Méthodes
	
	//Initialise les attributs du filtre (dont la liste des noms des futures checkboxes dans l'appli = liste des valeurs d'attributs en String)
	@SuppressWarnings("unchecked")
	public void init(String p_titre, String p_table, String p_champ) {
		
		this.titre = p_titre;
		this.table = p_table;
		this.champ = p_champ;
		String DAOType = "DAO_" + p_table;
		this.listeAffichageFiltre = ((ArrayList<String>)(DAOFactory.getDaoStr(DAOType).findByChamp(p_champ)));
		System.out.println("Bean filtre dit: liste recupérée de la DAO");
	}

	
	

}
