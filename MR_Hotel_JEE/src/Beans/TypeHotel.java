package Beans;

public class TypeHotel {
	
	//Attributs
	private int 	numeroTypeHotel;
	private String 	nomTypeHotel;
	
	//Constructeur
	public TypeHotel (){
		
	}
	
	public TypeHotel (int p_NumeroTypeHotel, String p_NomTypeHotel){
		this.numeroTypeHotel 	= p_NumeroTypeHotel;
		this.nomTypeHotel 		= p_NomTypeHotel;
	}

	//G & S
	public int getNumeroTypeHotel() {
		return numeroTypeHotel;
	}

	public void setNumeroTypeHotel(int numeroTypeHotel) {
		this.numeroTypeHotel = numeroTypeHotel;
	}

	public String getNomTypeHotel() {
		return nomTypeHotel;
	}

	public void setNomTypeHotel(String nomTypeHotel) {
		this.nomTypeHotel = nomTypeHotel;
	}
	
	//Méthodes
	
}
