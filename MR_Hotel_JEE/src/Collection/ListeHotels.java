package Collection;

import java.util.ArrayList;

import Beans.Hotel;

public class ListeHotels extends CollectionMetier<Hotel> {

	public ListeHotels(){
		super();
		collMetier = new ArrayList<Hotel>();
	}
	
	@Override
	public void Ajouter(Hotel elementAAjouter) {
		collMetier.add(elementAAjouter);
	}

	@Override
	public void Supprimer(Hotel ElementASupprimer) {
		collMetier.remove(ElementASupprimer);
	}
	
}
