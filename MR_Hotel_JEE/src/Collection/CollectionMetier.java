package Collection;
import java.util.ArrayList;


public abstract class CollectionMetier <T> {
	
	//Attributs
	protected ArrayList<T> collMetier;
	
	//Constructeur 
	public CollectionMetier () {
		
	}

	/**
	 * @param collMetier
	 */
	public CollectionMetier(ArrayList<T> collMetier) {
		this.collMetier = collMetier;
	}

	//G & S
	/**
	 * @return the collMetier
	 */
	public ArrayList<T> getCollMetier() {
		return this.collMetier;
	}

	/**
	 * @param collMetier the collMetier to set
	 */
	public void setCollMetier(ArrayList<T> collMetier) {
		this.collMetier = collMetier;
	}
	
	//Méthodes
	
	//Methode Abstraite <T>  Ajouter
	public abstract void Ajouter(T elementAAjouter);
	
	//Methode Abstraite <T> Supprimer
	public abstract void Supprimer (T ElementASupprimer);	

}
