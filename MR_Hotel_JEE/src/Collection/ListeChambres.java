package Collection;

import java.util.ArrayList;

import Beans.Chambre;

public class ListeChambres extends CollectionMetier<Chambre> {

	public ListeChambres(){
		super();
		collMetier = new ArrayList<Chambre>();
	}
	
	@Override
	public void Ajouter(Chambre elementAAjouter) {

		collMetier.add(elementAAjouter);
	}

	@Override
	public void Supprimer(Chambre ElementASupprimer) {
		
		collMetier.add(ElementASupprimer);
	}

}
