package Collection;

import java.util.ArrayList;

import Beans.TypeHotel;

public class ListeTypeHotel extends CollectionMetier<TypeHotel>{
	
	public ListeTypeHotel(){
		super();
		collMetier = new ArrayList<TypeHotel>();
	}
	
	@Override
	public void Ajouter(TypeHotel elementAAjouter) {
		// TODO Auto-generated method stub
		collMetier.add(elementAAjouter);
	}

	@Override
	public void Supprimer(TypeHotel ElementASupprimer) {
		// TODO Auto-generated method stub
		collMetier.remove(ElementASupprimer);
	}

}
