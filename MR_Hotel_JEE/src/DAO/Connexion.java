package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {
	
	// Constantes
	public static final String DRIVER_ORACLE = "oracle.jdbc.driver.OracleDriver";
	public static final String URL_ORACLE 	 = "jdbc:oracle:thin:@localhost:1521:xe";
		
	/// Attributs
	private static String		user 	   	= "MihaiDataHotel";
	private static String		passwd     	= "afpa123";
	private static Connection 	cxion	 	= null;

    
    // Constructeur
	public Connexion() {
	
	}

	// GETTERS
	public static Connection getCxion() {
		
		//Si la connection n'est pas cr��e, je la cr��e
		try {
			if ((cxion == null ||cxion.isClosed()) /*&& cxion.isValid()*/) { 
				Class.forName(DRIVER_ORACLE);
				cxion = DriverManager.getConnection(URL_ORACLE, user, passwd);
				System.out.println("Connexion OK!!");
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// quoiqu'il arrive je retourne la cxion
		return cxion;
	}
      
	public static  void fermerCxion (){
		try {
			cxion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
