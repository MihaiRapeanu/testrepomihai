package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Beans.CategorieChambre;
import Beans.Chambre;
import Beans.Hotel;
import Collection.CollectionMetier;

public class DAOCategorieChambre implements iDAO<CategorieChambre> {

	@Override
	public CategorieChambre findById(int p_IdARecherche) {
		
		CategorieChambre findedCatChambre = new CategorieChambre();
		
		try {
			Statement statement =Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM CATEGORIE WHERE NUCAT = " + p_IdARecherche);
			
			while (resultat.next()) {
				findedCatChambre.setNumeroCategorie(resultat.getInt(1));
				findedCatChambre.setNomCategorie(resultat.getString(2));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findedCatChambre;
	}

	@Override
	public CollectionMetier<CategorieChambre> findAll() {
		// Xavier PARASKIOVA
		return null;
	}

	@Override
	public ArrayList<String> findByChamp(String p_champ) {
		
		ArrayList<String> listeChamp = new ArrayList<String>();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT " + p_champ + " FROM CATEGORIE");
			
			while (resultat.next()) {
				listeChamp.add((String) resultat.getString(1));
				System.out.println("DAO Categorie dit: ajout liste");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listeChamp;
	}

	@Override
	public CollectionMetier<CategorieChambre> findRange(int range) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectionMetier<CategorieChambre> findByCriteria(String champ, String valeur) {
		// TODO Auto-generated method stub
		return null;
	}

}
