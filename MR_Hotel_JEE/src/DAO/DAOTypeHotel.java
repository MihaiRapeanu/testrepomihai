package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Beans.TypeHotel;
import Collection.CollectionMetier;
import Collection.ListeTypeHotel;

public class DAOTypeHotel implements iDAO<TypeHotel> {

	@Override
	public TypeHotel findById(int p_IdARecherche) {
		
		TypeHotel findedTypeHotel = new TypeHotel();
		try {
			Statement statement =Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM TYPE_HOTEL WHERE NUTYPE = " + p_IdARecherche);
			while (resultat.next()) {
				findedTypeHotel.setNumeroTypeHotel(resultat.getInt(1));
				findedTypeHotel.setNomTypeHotel(resultat.getString(2));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findedTypeHotel;
	}

	@Override
	public CollectionMetier<TypeHotel> findAll() {
		// Xavier PARASKIOVA
		
		CollectionMetier<TypeHotel> collectionARetourner = new ListeTypeHotel();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM TYPE_HOTEL");
			
			while (resultat.next()) {
				collectionARetourner.Ajouter(new TypeHotel(resultat.getInt(1),
													   	   resultat.getString(2)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();	
		}		
		
		return collectionARetourner;
	}

	@Override
	public ArrayList<String> findByChamp(String p_champ) {

		ArrayList<String> listeChamp = new ArrayList<String>();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT " + p_champ + " FROM TYPE_HOTEL");
			
			while (resultat.next()) {
				listeChamp.add((String) resultat.getString(1));
				System.out.println("DAO TypeHotel dit: ajout liste");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listeChamp;
	}

	@Override
	public CollectionMetier<TypeHotel> findRange(int range) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectionMetier<TypeHotel> findByCriteria(String champ, String valeur) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
