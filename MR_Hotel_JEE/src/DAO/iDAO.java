package DAO;

import java.util.ArrayList;

import Collection.CollectionMetier;

public interface iDAO<T> {
	
	/**
	 * Description des m�thodes du CRUD
	 */
	//public abstract T        				create     		(T p_ObjetACreer);
	public abstract T						findById  		(int p_IdARecherche);
	public abstract CollectionMetier<T>		findAll			();
	public abstract CollectionMetier<T> 	findRange		(int range);
	public abstract ArrayList<String>		findByChamp		(String p_champ);
	public abstract CollectionMetier<T>		findByCriteria	(String champ, String valeur);
	//public abstract T		  				update    		(T pObjetAModifier);
	//public abstract Boolean 				delete      	(T pObjetASupprimer);
}
