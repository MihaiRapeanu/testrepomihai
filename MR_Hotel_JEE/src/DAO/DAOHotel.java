package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Beans.Hotel;
import Beans.TypeHotel;
import Collection.CollectionMetier;
import Collection.ListeHotels;

public class DAOHotel implements iDAO<Hotel> {

	@Override
	public Hotel findById(int p_IdARecherche) {
		Hotel findedHotel = new Hotel();
		try {
			Statement statement =Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM HOTEL WHERE NUHOTEL = " + p_IdARecherche);
			while (resultat.next()) {
				findedHotel.setNumeroHotel(resultat.getInt(1));
				findedHotel.setTypeHotel((TypeHotel) (DAOFactory.getDao(TypeDAO.DAO_TYPE_HOTEL).findById(resultat.getInt(2))));
				findedHotel.setNomHotel(resultat.getString(3));
				findedHotel.setAdresseHotel(resultat.getString(4));
				findedHotel.setCodePostalHotel(resultat.getString(5));
				findedHotel.setVilleHotel(resultat.getString(6));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findedHotel;
	}

	@Override
	public CollectionMetier<Hotel> findAll() {
		
		CollectionMetier<Hotel> collectionARetourner = new ListeHotels();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM HOTEL");
			
			while (resultat.next()) {
				collectionARetourner.Ajouter(new Hotel(resultat.getInt(1),
													   ((TypeHotel) (DAOFactory.getDao(TypeDAO.DAO_TYPE_HOTEL).findById(resultat.getInt(2)))),
													   resultat.getString(3),
													   resultat.getString(4),
													   resultat.getString(5),
													   resultat.getString(6)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();	
		}		
		
		return collectionARetourner;
	}

	@Override
	public ArrayList<String> findByChamp(String p_champ) {
		
		ArrayList<String> listeChamp = new ArrayList<String>();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT " + p_champ + " FROM TYPE_HOTEL");
			
			while (resultat.next()) {
				listeChamp.add((String) resultat.getString(1));
				System.out.println("DAO TypeHotel dit: ajout liste");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listeChamp;
	}

	@Override
	public CollectionMetier<Hotel> findRange(int range) {

		CollectionMetier<Hotel> collectionARetourner = new ListeHotels();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM HOTEL WHERE ROWNUM < " + (range + 1));
			
			while (resultat.next()) {
				collectionARetourner.Ajouter(new Hotel(resultat.getInt(1),
													   ((TypeHotel) (DAOFactory.getDao(TypeDAO.DAO_TYPE_HOTEL).findById(resultat.getInt(2)))),
													   resultat.getString(3),
													   resultat.getString(4),
													   resultat.getString(5),
													   resultat.getString(6)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();	
		}		
		
		return collectionARetourner;
	}

	@Override
	public CollectionMetier<Hotel> findByCriteria(String champ, String valeur) {
		// TODO Auto-generated method stub
		return null;
	}

}
