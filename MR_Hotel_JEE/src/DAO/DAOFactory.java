package DAO;

public class DAOFactory  {
	
	@SuppressWarnings("rawtypes")
	public static iDAO getDao (TypeDAO type) {
		
		switch (type) {
		case DAO_CATEGORIE :
			return new DAOCategorieChambre();
			
		case DAO_CHAMBRE :
			return new DAOChambre();
			
		case DAO_HOTEL :
			return new DAOHotel();
			
		case DAO_TYPE_HOTEL :
			return new DAOTypeHotel();
			
		default :
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static iDAO getDaoStr (String type) {
		
		switch (type) {
		case "DAO_CATEGORIE" :
			return new DAOCategorieChambre();
			
		case "DAO_CHAMBRE" :
			return new DAOChambre();
			
		case "DAO_HOTEL" :
			return new DAOHotel();
			
		case "DAO_TYPE_HOTEL" :
			return new DAOTypeHotel();
			
		default :
			return null;
		}
	}
	
}
