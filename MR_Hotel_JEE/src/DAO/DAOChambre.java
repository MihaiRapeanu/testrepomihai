package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Beans.CategorieChambre;
import Beans.Chambre;
import Beans.Hotel;
import Collection.CollectionMetier;
import Collection.ListeChambres;

public class DAOChambre implements iDAO<Chambre> {

	@Override
	public Chambre findById(int p_IdARecherche) {
		Chambre findedChambre = new Chambre();
		try {
			Statement statement =Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM CHAMBRE WHERE NUCHAMBRE = " + p_IdARecherche);
			while (resultat.next()) {
				findedChambre.setNumeroDeChambre(resultat.getInt(1));
				findedChambre.setCategorieChambre((CategorieChambre) (DAOFactory.getDao(TypeDAO.DAO_CATEGORIE).findById(resultat.getInt(2))));
				findedChambre.setHotelAssoChambre((Hotel) 			 (DAOFactory.getDao(TypeDAO.DAO_HOTEL).findById(resultat.getInt(3))));
			
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return findedChambre;
	}

	@Override
	public CollectionMetier<Chambre> findAll() {
		CollectionMetier<Chambre> collectionARetourner = new ListeChambres();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM CHAMBRE");
			
			while (resultat.next()) {
				collectionARetourner.Ajouter(new Chambre(resultat.getInt(1),
														 ((CategorieChambre) (DAOFactory.getDao(TypeDAO.DAO_CATEGORIE).findById(resultat.getInt(3)))),
														 ((Hotel) 			 (DAOFactory.getDao(TypeDAO.DAO_HOTEL).findById(resultat.getInt(2))))));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();	
		}		
		
		return collectionARetourner;
	}

	@Override
	public ArrayList<String> findByChamp(String p_champ) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectionMetier<Chambre> findRange(int range) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectionMetier<Chambre> findByCriteria(String champ, String valeur) {
		
		CollectionMetier<Chambre> collectionARetourner = new ListeChambres();
		
		try {
			Statement statement = Connexion.getCxion().createStatement();
			ResultSet resultat = statement.executeQuery("SELECT * FROM CHAMBRE WHERE " + champ + " = " + valeur);
			
			while (resultat.next()) {
				collectionARetourner.Ajouter(new Chambre(resultat.getInt(1),
														 ((CategorieChambre) (DAOFactory.getDao(TypeDAO.DAO_CATEGORIE).findById(resultat.getInt(3)))),
														 ((Hotel) 			 (DAOFactory.getDao(TypeDAO.DAO_HOTEL).findById(resultat.getInt(2))))));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();	
		}		
		
		return collectionARetourner;
	}


}
