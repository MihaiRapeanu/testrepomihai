
package Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.Filtre;
import Beans.Hotel;
import DAO.DAOFactory;
import DAO.TypeDAO;

/**
 * Servlet implementation class Accueil
 */
@WebServlet(name="/Accueil", urlPatterns= {"/Accueil"})
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Accueil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		process(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Point d'entrée de l'application
		//System.out.println("Dans servlet accueil");
		
		//Declaration d'une liste de Filtres
		ArrayList<Filtre> listeFiltresAffichage = new ArrayList<Filtre>();
		
		//création des filtres
		Filtre filtreCategorie = new Filtre();
		filtreCategorie.init("Tri par catégories de chambre", "CATEGORIE", "NOMCAT");
		
		Filtre filtreTypeHotel = new Filtre();
		filtreTypeHotel.init("Tri par types d'hotel", "TYPE_HOTEL", "NOMTYPE");
		
		//ajout des filtres à la liste de filtres
		listeFiltresAffichage.add(filtreCategorie);
		listeFiltresAffichage.add(filtreTypeHotel);
		
		//ajout de la liste de filtres dans la requete
		request.setAttribute("listeFiltres", listeFiltresAffichage);
		System.out.println("objet filtre dans request");
		
		//Recuperation des 20 premiers hotels + mise en attribut de requete
		ArrayList<Hotel> listeHotel = new ArrayList<Hotel>();
		listeHotel = (ArrayList<Hotel>) DAOFactory.getDao(TypeDAO.DAO_HOTEL).findRange(20).getCollMetier();
		request.setAttribute("listeHotels", listeHotel);

		this.getServletContext().getRequestDispatcher("/WEB-INF/vuePrincipale.jsp").forward(request, response);
		
	}

}
