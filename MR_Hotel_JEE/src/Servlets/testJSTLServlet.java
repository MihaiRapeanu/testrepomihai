package Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.Chambre;
import Beans.Hotel;
import DAO.DAOFactory;
import DAO.TypeDAO;

/**
 * Servlet implementation class testJSTLServlet
 */
@WebServlet(name="/testJSTLServlet", urlPatterns="/testJSTLServlet")
public class testJSTLServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public testJSTLServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		process(request, response);
	}
	
	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("servlet");

		request.setAttribute("beanHotel", DAOFactory.getDao(TypeDAO.DAO_HOTEL).findById(Integer.parseInt(request.getParameter("rechercheIDHotel"))) );
	
		request.setAttribute("listeChambres", DAOFactory.getDao(TypeDAO.DAO_CHAMBRE).findByCriteria("NUHOTEL", request.getParameter("rechercheIDHotel")).getCollMetier());
		
		getServletContext().getRequestDispatcher("/testsJSTL.jsp").forward(request, response);
		
	}

}
