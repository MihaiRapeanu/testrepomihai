package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.Filtre;

/**
 * Servlet implementation class actionFiltre
 */
@WebServlet(name="/ActionFiltre", urlPatterns="/ActionFiltre")
public class ActionFiltre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActionFiltre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		process(request, response);
	}
	
	@SuppressWarnings("unchecked")
	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//servlet appellée par le formulaire de tri par filtres
		
		PrintWriter out = response.getWriter();
		out.println("Servlet action formulaire filtres");
		out.println("<br/>");
		
		//Recuperation de la liste de filtre dans session
		ArrayList<Filtre> listeFiltres = (ArrayList<Filtre>) request.getSession().getAttribute("listeFiltres");
		
		//Pour chaque filtre de la liste de filtres
		for (Filtre filtre : listeFiltres) {
			
			//Je récupère la liste des nom de checkbox dans la vue
			ArrayList<String> listeStringFiltre = (ArrayList<String>) filtre.getListeChampARetourner();
			
			//pour chaque nom de checkbox
			for (String str : listeStringFiltre) {
							
					//si sa valeur n'est pas nulle (passée dans la methodes post lors de la validation du formulaire)
					if (request.getParameter(str) != null)
					{
						out.println("case cochée: " + str);
						out.println("<br/>");
						//ici je veux récupérer les valeurs des cases cochées, créé une requete selon les cases cochées, recuperer une liste d'hotel a afficher selon la requete
							
					} 
				
			}
			
		}
		
	}

}
